import sqlite3 as sql

#connect to SQLite
con = sql.connect('db_web.db')

#Create a Connection
cur = con.cursor()

#Drop users table if already exsist.
cur.execute("DROP TABLE IF  EXISTS users")



#Create users table  in db_web database
sql ='''CREATE TABLE "users" (
	"UID"	INTEGER,
	"UNAME"	TEXT,
	"CONTACT"	TEXT,
	"EMAIL"	TEXT,
	"CPF"	TEXT UNIQUE,
	PRIMARY KEY("UID" AUTOINCREMENT)
	
)'''




cur.execute(sql)

cur.execute("DROP TABLE IF EXISTS login")

sql_pws ='''CREATE TABLE "login" (
	"ID_PAS"	INTEGER,
    "EMAIL" TEXT,
	"PSW"	INTEGER,
	PRIMARY KEY("ID_PAS")
)'''
	
cur.execute(sql_pws)




#commit changes
con.commit()

#close the connection
con.close()
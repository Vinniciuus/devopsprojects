
from flask import Flask,render_template,request,redirect,url_for,flash
import sqlite3 as sql
from flask import Blueprint, render_template, redirect, url_for, request
from werkzeug.security import generate_password_hash, check_password_hash


app=Flask(__name__)

@app.route("/")
@app.route("/index")
def index():
    con=sql.connect("db_web.db")
    con.row_factory=sql.Row
    cur=con.cursor()
    cur.execute("select * from users")
    data=cur.fetchall()
    return render_template("index.html",datas=data)
    


    
@app.route('/login')
def login():
    return render_template('login.html')
    
    
@app.route('/orcamento')
def orcamento():
    return render_template('orcamento.html')
    
    
    

    
    
# @app.route('/autenticar', methods=['POST'])
# def autenticar():
    # login_txt = request.form.get('login_txt')
    # pass_txt = request.form.get('pass_txt')
    # return "usuario: {} senha: {}".format(login_txt, pass_txt)
    

@app.route("/add_user",methods=['POST','GET'])
def add_user():
    if request.method=='POST':
        uname=request.form['uname']
        contact=request.form['contact']
        email=request.form['email']
        con=sql.connect("db_web.db")
        cur=con.cursor()
        cur.execute("insert into users(UNAME,CONTACT,EMAIL) values (?,?,?)",(uname,contact,email))
        con.commit()
        flash('User Added','success')
        return redirect(url_for("index"))
    return render_template("add_user.html")
    



@app.route("/cadastro",methods=['POST','GET'])
def cadastro():
    if request.method=='POST':
        user_email=request.form['user_email']
        user_senha=request.form['user_senha']
        
        
        con=sql.connect("db_web.db")
        cur=con.cursor()
        cur.execute("insert into login(EMAIL, PSW) values (?,?)",(user_email, user_senha))
        con.commit()
        flash('login Added','success')
        return redirect(url_for("login"))
    return render_template("cadastro.html")





  
# @app.route("/autenticar",methods=['POST','GET'])
# def autenticar():

    # if request.method=='POST':
        # EMAIL=request.form['login_txt']
        # PWS=request.form['pass_txt']
        
        # con=sql.connect("db_web.db")
        # cur=con.cursor()
        # cur.execute("select * from login where EMAIL=? ",(EMAIL))
        # con.commit()
        # flash('User select','success')
        # return redirect(url_for("autenticar"))
        
    # EMAIL=request.form['login_txt']
    # PWS=request.form['pass_txt']
    # con=sql.connect("db_web.db")
    # con.row_factory=sql.Row
    # cur=con.cursor()
    # cur.execute("select * from login where EMAIL",(EMAIL))
    # flash('User select','success')
    # return redirect(url_for("index"))  
    
@app.route('/autenticar', methods=['POST'])
def autenticar():
    email = request.form.get('login_txt')
    

    user = User.query.filter_by(email=email).first()

    if user:
        return redirect(url_for('autenticar'))

    
    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))

    
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('autenticar'))
    
    
    

@app.route("/edit_user/<string:uid>",methods=['POST','GET'])
def edit_user(uid):
    if request.method=='POST':
        uname=request.form['uname']
        contact=request.form['contact']
        email=request.form['email']
        con=sql.connect("db_web.db")
        cur=con.cursor()
        cur.execute("update users set UNAME=?,CONTACT=?, EMAIL=? where UID=?",(uname,contact,email, uid))
        con.commit()
        flash('User select','success')
        return redirect(url_for("index"))
    con=sql.connect("db_web.db")
    con.row_factory=sql.Row
    cur=con.cursor()
    cur.execute("select * from users where UID=?",(uid,))
    data=cur.fetchone()
    return render_template("edit_user.html",datas=data)
    
@app.route("/delete_user/<string:uid>",methods=['GET'])
def delete_user(uid):
    con=sql.connect("db_web.db")
    cur=con.cursor()
    cur.execute("delete from users where UID=?",(uid,))
    con.commit()
    flash('User Deleted','warning')
    return redirect(url_for("index"))
    
if __name__=='__main__':
    app.secret_key='admin123'
    app.run(debug=True)